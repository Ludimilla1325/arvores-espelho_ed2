// In-ordem
#include <malloc.h>
#include <stdio.h>

typedef struct tree
{
    char info;
    struct tree *left;
    struct tree *right;
} node;

node *createNode(char info)
{
    node *newNode = (node *)malloc(sizeof(node));
    newNode->info = info;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

int checkIfIsMirror(node *root, node *mirror)
{
    if (root != NULL)
    {
        int checkLeft = checkIfIsMirror(root->left, mirror->right);
        int checkRight = checkIfIsMirror(root->right, mirror->left);
        if (root->info == mirror->info && checkRight && checkLeft)
        {
            printf("It's mirror");
            return 1;
        }
        else
        {
            printf("It's not mirror");
            return 0;
        };
    }
    printf("\n");
    return 1;
}

void inOrder(node *root)
{
    if (root == NULL)
        return;
    inOrder(root->left);
    printf("%c ", root->info);
    inOrder(root->right);
}

void treeFree(node *root)
{
    if (root != NULL)
    {
        treeFree(root->left);
        treeFree(root->right);
        free(root);
    }
}

void mirrorify(node *root, node **mirror)
{
    if (root == NULL)
    {
        mirror = NULL;
        return;
    }
    *mirror = createNode(root->info);
    mirrorify(root->left, &((*mirror)->right));
    mirrorify(root->right, &((*mirror)->left));
}

void main()
{

    node *tree = createNode('A');
    tree->left = createNode('B');
    tree->right = createNode('C');
    tree->left->left = createNode('D');
    tree->right->left = createNode('E');
    tree->right->right = createNode('F');

    printf("inOrder of original tree: ");
    inOrder(tree);
    node *mirror = NULL;
    mirrorify(tree, &mirror);

    printf("\ninOrder of mirror tree: ");
    inOrder(mirror);

    checkIfIsMirror(tree, mirror);

    treeFree(tree);
    treeFree(mirror);
}